﻿using Convoccatoria1_2M2.entities;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convoccatoria1_2M2.model
{
    class ClienteModel
    {
        private static List<Cliente> ListClientes = new List<Cliente>();

        public static List<Cliente> GetListCliente()
        {
            return ListClientes;
        }

        public static void Populate()
        {
      
            ListClientes = JsonConvert.DeserializeObject<List<Cliente>>(System.Text.Encoding.Default.GetString(Convoccatoria1_2M2.Properties.Resources.cliente_data));
        }
    }

}


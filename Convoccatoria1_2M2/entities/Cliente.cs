﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Convoccatoria1_2M2.entities
{
    class Cliente
    {
        private int id;
        private string cedula;
        private string nombres;
        private string apellidos;
    private string celular;
        private string correo;
        private string direccion;
        private string municipio;
        private string departamento;

        public Cliente(int id, string cedula, string nombres, string apellidos, string celular, string correo, string direccion, string municipio, string departamento)
        {
            this.id = id;
            this.cedula = cedula;
            this.nombres = nombres;
            this.apellidos = apellidos;
            this.celular = celular;
            this.correo = correo;
            this.direccion = direccion;
            this.municipio = municipio;
            this.departamento = departamento;
        }

        public int Id { get => id; set => id = value; }
        public string Cedula { get => cedula; set => cedula = value; }
        public string Nombres { get => nombres; set => nombres = value; }
        public string Apellidos { get => apellidos; set => apellidos = value; }
        public string Celular { get => celular; set => celular = value; }
        public string Correo { get => correo; set => correo = value; }
        public string Direccion { get => direccion; set => direccion = value; }
        public string Municipio { get => municipio; set => municipio = value; }
        public string Departamento { get => departamento; set => departamento = value; }
    }
}

﻿namespace Convoccatoria1_2M2
{
    partial class FrmMain
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.CATALOGOSToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EXTINTORESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.CLIENTESToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.REPORTESToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.NUEVOREPORTEToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DsCatalogo = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DsCatalogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CATALOGOSToolStripMenuItem,
            this.REPORTESToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // CATALOGOSToolStripMenuItem
            // 
            this.CATALOGOSToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EXTINTORESToolStripMenuItem,
            this.CLIENTESToolStripMenuItem1});
            this.CATALOGOSToolStripMenuItem.Name = "CATALOGOSToolStripMenuItem";
            this.CATALOGOSToolStripMenuItem.Size = new System.Drawing.Size(79, 20);
            this.CATALOGOSToolStripMenuItem.Text = "CATALOGO";
            // 
            // EXTINTORESToolStripMenuItem
            // 
            this.EXTINTORESToolStripMenuItem.Name = "EXTINTORESToolStripMenuItem";
            this.EXTINTORESToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.EXTINTORESToolStripMenuItem.Text = "EXTINTORES";
            // 
            // CLIENTESToolStripMenuItem1
            // 
            this.CLIENTESToolStripMenuItem1.Name = "CLIENTESToolStripMenuItem1";
            this.CLIENTESToolStripMenuItem1.Size = new System.Drawing.Size(180, 22);
            this.CLIENTESToolStripMenuItem1.Text = "CLIENTES";
            this.CLIENTESToolStripMenuItem1.Click += new System.EventHandler(this.CLIENTESToolStripMenuItem1_Click);
            // 
            // REPORTESToolStripMenuItem
            // 
            this.REPORTESToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.NUEVOREPORTEToolStripMenuItem});
            this.REPORTESToolStripMenuItem.Name = "REPORTESToolStripMenuItem";
            this.REPORTESToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.REPORTESToolStripMenuItem.Text = "REPORTES";
            // 
            // NUEVOREPORTEToolStripMenuItem
            // 
            this.NUEVOREPORTEToolStripMenuItem.Name = "NUEVOREPORTEToolStripMenuItem";
            this.NUEVOREPORTEToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.NUEVOREPORTEToolStripMenuItem.Text = "NUEVO REPORTE";
            // 
            // DsCatalogo
            // 
            this.DsCatalogo.DataSetName = "NewDataSet";
            this.DsCatalogo.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dataTable1.TableName = "Cliente";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Id";
            this.dataColumn1.DataType = typeof(int);
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Cedula";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Nombres";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Apellidos";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "Celular";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "Correo";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "Municipio";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "Direccion";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "Departamento";
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FrmMain";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FrmMain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DsCatalogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem CATALOGOSToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EXTINTORESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem CLIENTESToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem REPORTESToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem NUEVOREPORTEToolStripMenuItem;
        private System.Data.DataSet DsCatalogo;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
    }
}


﻿using Convoccatoria1_2M2.entities;
using Convoccatoria1_2M2.model;
using Convoccatoria1_2M2.views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convoccatoria1_2M2
{
    public partial class FrmMain : Form
    {

      //  private DataTable dtCatalogo;
        public FrmMain()
        {
            InitializeComponent();
        }

        private void CLIENTESToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmGestionCliente frmgc = new FrmGestionCliente();
            frmgc.MdiParent = this;
            frmgc.DsClientes = DsCatalogo;
            frmgc.Show();
            
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
         //   dtCatalogo = DsCatalogo.Tables["Cliente"];

            ClienteModel.Populate();
            //MessageBox.Show(this, "wi");
            foreach(Cliente client in ClienteModel.GetListCliente())
            {
                DataRow drCliente = DsCatalogo.Tables["Cliente"].NewRow();
                drCliente["Id"] = client.Id;
                drCliente["Cedula"] = client.Cedula;
                drCliente["Nombres"] = client.Nombres;
                drCliente["Apellidos"] = client.Apellidos;
                drCliente["Celular"] = client.Celular;
                drCliente["Correo"] = client.Correo;
                drCliente["Direccion"] = client.Direccion;
                drCliente["Municipio"] = client.Municipio;
                drCliente["Departamento"] = client.Departamento;

                DsCatalogo.Tables["Cliente"].Rows.Add(drCliente);
                drCliente.AcceptChanges();
               // MessageBox.Show(this, "wi");
            }

            MessageBox.Show(this, "wi");
        }
    }
}

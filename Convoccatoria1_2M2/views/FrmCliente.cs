﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Convoccatoria1_2M2.views
{
    public partial class FrmCliente : Form
    {

        private DataTable tblClientes;
        private DataSet dsClientes;
        private BindingSource bsClientes;
        private DataRow drCliente;



        public FrmCliente()
        {
            InitializeComponent();
            bsClientes = new BindingSource();
        }

        public FrmCliente(DataTable tblClientes, DataSet dsClientes)
        {
            this.TblClientes = tblClientes;
            this.DsClientes = dsClientes;
        }

        public DataRow DrCliente
        {
            set
            {
                drCliente = value;
                mskCedula.Text = drCliente["Cedula"].ToString();
                txtNombres.Text = drCliente["Nombres"].ToString();
                txtApellidos.Text = drCliente["Apellidos"].ToString();
                mskCelular.Text = drCliente["Celular"].ToString();
                txtCorreo.Text = drCliente["Correo"].ToString();
                txtDireccion.Text = drCliente["Direccion"].ToString();
                txtMunicipio.Text = drCliente["Municipio"].ToString();
                cmbDepartamento.Text = drCliente["Departamento"].ToString();
            }
        }

        public DataSet DsClientes
        {
            get
            {
                return dsClientes;
            }

            set
            {
                dsClientes = value;
            }
        }

        public DataTable TblClientes
        {
            get
            {
                return tblClientes;
            }

            set
            {
                tblClientes = value;
            }
        }

        private void FrmCliente_Load(object sender, EventArgs e)
        {
            bsClientes.DataSource = DsClientes;
            bsClientes.DataMember = DsClientes.Tables["Cliente"].TableName;
        }

        private void BtnSave_Click(object sender, EventArgs e)
        {
            string cedula, nombres, apellidos, celular, correo, direccion,municipio,departamento;
            cedula = mskCedula.Text;
            nombres = txtNombres.Text;
            apellidos = txtApellidos.Text;
            celular = mskCelular.Text;
            correo = txtCorreo.Text;
            direccion = txtDireccion.Text;
            municipio = txtMunicipio.Text;
            departamento = cmbDepartamento.SelectedText;
            if (drCliente != null)
            {
                DataRow drNew = TblClientes.NewRow();

                int index = TblClientes.Rows.IndexOf(drCliente);
                drNew["Id"] = drCliente["Id"];
                drNew["Cédula"] = cedula;
                drNew["Nombres"] = nombres;
                drNew["Apellidos"] = apellidos;
                drNew["celular"] = celular;
                drNew["Correo"] = correo;
                drNew["Dirección"] = direccion;
                drNew["Municipio"] = municipio;
                drNew["Departamento"] = departamento;


                TblClientes.Rows.RemoveAt(index);
                TblClientes.Rows.InsertAt(drNew, index);
                TblClientes.Rows[index].AcceptChanges();
                TblClientes.Rows[index].SetModified();
            }
            else
            {
                TblClientes.Rows.Add(TblClientes.Rows.Count + 1, cedula, nombres, apellidos, celular, correo, direccion,municipio,departamento);
            }

            Dispose();
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

    }
}

